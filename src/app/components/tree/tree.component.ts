import { Component, OnInit } from '@angular/core';
import { TreeNode } from 'primeng/components/common/treenode';
import { ApiService } from 'src/app/services/apiService';
@Component({
  selector: 'app-tree',
  templateUrl: './tree.component.html',
  styleUrls: ['./tree.component.scss']
})
export class TreeComponent implements OnInit {
 
  files: TreeNode[] = [
    {
      "label": "Documents",
      "data": "Documents Folder",
      "children": [{
        "label": "Work",
        "data": "Work Folder",
        "children": [{ "label": "Expenses.doc", "icon": "fa fa-file-word-o", "data": "Expenses Document" }, { "label": "Resume.doc", "icon": "fa fa-file-word-o", "data": "Resume Document" }]
      },
      {
        "label": "Home",
        "data": "Home Folder",
        "children": [{ "label": "Invoices.txt", "icon": "fa fa-file-word-o", "data": "Invoices for this month" }]
      }]
    },
    {
      "label": "Pictures",
      "data": "Pictures Folder",
      "children": [
        { "label": "barcelona.jpg", "icon": "fa fa-file-image-o", "data": "Barcelona Photo" },
        { "label": "logo.jpg", "icon": "fa fa-file-image-o", "data": "PrimeFaces Logo" },
        { "label": "primeui.png", "icon": "fa fa-file-image-o", "data": "PrimeUI Logo" }]
    },
    {
      "label": "Movies",
      "data": "Movies Folder",
      "children": [{
        "label": "Al Pacino",
        "data": "Pacino Movies",
        "children": [{ "label": "Scarface", "icon": "fa fa-file-video-o", "data": "Scarface Movie" }, { "label": "Serpico", "icon": "fa fa-file-video-o", "data": "Serpico Movie" }]
      },
      {
        "label": "Robert De Niro",
        "data": "De Niro Movies",
        "children": [{ "label": "Goodfellas", "icon": "fa fa-file-video-o", "data": "Goodfellas Movie" }, { "label": "Untouchables", "icon": "fa fa-file-video-o", "data": "Untouchables Movie" }]
      }]
    }
  ];
  selectedFiles: TreeNode[] = [];
  
  selectedFilesList: any[] = [];
  filesCount = 0;

  selectedText = 'Not yet selected';

  

  constructor(private apiService:ApiService) { }

  ngOnInit() {
this.getData()
  
    this.countTotal(this.files);
  }
  getData(){
    this.apiService.get().subscribe(res=>{
      var data= this.convertToTree(res);
  this.files=data;
    })
  }
  convertToTree(data){
    data =data.filter((value, index, self) =>
  index === self.findIndex((t) => (
    t.client_company_name == value.client_company_name && t.server_group_name == value.server_group_name
    &&  t.server_name == value.server_name && t.instance_name == value.instance_name && t.database_name == value.database_name
  ))
)

    let b = [];
    var database_name=[];
data.map(item => {
 
  let obj = b.findIndex(o => {
  var de= o.label.localeCompare(item.client_company_name)
   return de==0?true:false
  })


  if (obj == -1) {
    let d = {
      label: item.client_company_name,
      data:item.client_company_name,
      children: []
    }
    d.children.push({
      label:item.server_group_name,
      data:item.server_group_name,
      children:[{
        label:item.server_name,
        data:item.server_name,
        children:[{
          label:item.instance_name,
          data:item.instance_name,
          children:[{
            label:item.database_name,
            data:item.database_name
          }]
        }]
      }]
    })
    b.push(d)


  }
})
 console.log(data)
  for (let i=0;i<b.length;i++){
    let c=b[i].children;
   

   
    for (let j=0;j<c.length;j++){
        
       data.map(o => {
      
        if(o.client_company_name==b[i].label){
    
        if(o.server_group_name==b[i].children[j].label){
          
        }
        else{
        
          let obj1 = c.findIndex(k => {
          
            var de= k.label.localeCompare(o.server_group_name)
             return de==0?true:false
            })
          
            if(obj1==-1){
           
              c.push({
                label:o.server_group_name,
                data:o.server_group_name,
                children:[{
                  label:o.server_name,
                  data:o.server_name,
                  children:[{
                    label:o.instance_name,
                    data:o.instance_name,
                    children:[{
                      label:o.database_name,
                      data:o.database_name
                    }]
                  }]
                }]
              })
            }
        }
        }
         var de= o.server_group_name.localeCompare(c[j].label)
          return de==0?true:false
         })

      let d=b[i].children[j].children
    

      for (let k=0;k<d.length;k++){
        
        data.map(o => {
      
          if(o.client_company_name==b[i].label){
          if(o.server_group_name==b[i].children[j].label && d.length>0){
            
           if(o.server_name==d[k].label)
           {
           
          }
          else{
           
            let obj1 = d.findIndex(k => {
              var de= k.label.localeCompare(o.server_name)
               return de==0?true:false
              })
            
              if(obj1==-1){
            
                d.push({
                  label:o.server_name,
                  data:o.server_name
                })
              }
          }
       
          }
   
        }
         
           })
     
           let e=d[k].children;
           console.log(e.length)
        for (let l=0;l<e.length;l++){
        
          data.findIndex(o => {
      
            if(o.client_company_name==b[i].label){
            if(o.server_group_name==b[i].children[j].label){
             if(o.server_name==d[k].label)
             {
              if(o.instance_name==e[k].label)
              {
              
             }
             else{
           
              let obj1 = e.findIndex(k => {
                var de= k.label.localeCompare(o.instance_name)
                 return de==0?true:false
                })
                if(obj1==-1){
                  e.push({
                    label:o.instance_name,
                    data:o.instance_name
                  })
                }
             }


            }
            
         
            }
          }
         
             })
             
             let f=e[l].children;
          for (let m=0;m<f.length;m++){
            data.findIndex(o => {
       
              if(o.client_company_name==b[i].label){
              //  console.log(o.server_name)
            // console.log(o.server_group_name)
            // console.log(b[i].children[j].label)
            // console.log(o.server_group_name==b[i].children[j].label || o.server_group_name == '')
              if(o.server_group_name==b[i].children[j].label || o.server_group_name == ''){
               
               if(o.server_name==d[k].label)
               {
               
                if(o.instance_name==e[k].label)
                {
               

                  if(o.database_name==f[m].label)
                  {
                   
                 
                 }
                 else{
                 
                  let obj1 = f.findIndex(k => {
                    var de= k.label.localeCompare(o.database_name)
                     return de==0?true:false
                    })
                  
                    if(obj1==-1){
                    
                      f.push({
                        label:o.database_name,
                        data:o.database_name
                      })
                    }
                 }
               

               }
              
  
  
              }
              
           
              }
            }
           
               })
          }
        }
      }
    }


  }


return b;
  }


  nodeSelected(nodeData: any) {
    this.selectedText = nodeData;
   
    this.selectedFilesList =  this.selectedFiles.filter(x => {
      if(!x.children)  { 
        return true  
      } else {
        false
      }
    });
  }

  doSomething(event, item) {
    event.stopPropagation();
    // as before you had
    this.selectedText = item;
  }

  countTotal(list: any) {
    if(list) {
      for (let index = 0; index < list.length; index++) {
        this.recursionChildNode(list[index]);
      }
    }
  }

  recursionChildNode(node: any) {
    if(!node.children) {
      this.filesCount++;
      return;
    } 
    
    for(let i=0 ; i < node.children.length ; i++) {
      this.recursionChildNode(node.children[i]);
    }
  }




}
